<?php

	class Database{

		public $host     = 'DB_HOST';
		public $user     = 'DB_USER';
		public $pass     = 'DB_PASS';
		public $db_name  = 'DB_NAME';


		public $link;
		
		public $error;

		private function connectDB(){
		  $this->link = new mysqli($this->host, $this->user, $this->pass, 
		  $this->db_name);
		  if (!$this->link) {
		   $this->error = "Connection fail.".$this->link->connect_error;
		  }
		 }

		 //Insert Data
		 public function insert($query){
		  $insert_row = $this->link->query($query) or 
		  die($this->link->error.__LINE__);
		  if ($insert_row) {
		   return $insert_row;
		  } else {
		   return false;
		  }
		 }

		 // Select Data
		 public function select($query){
		  $result = $this->link->query($query) or 
		  die($this->link->error.__LINE__);
		  if ($result->num_rows > 0) {
		   return $result;
		  } else {
		   return false;
		  }
		 }
	}
	
?>