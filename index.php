<?php

	include 'lib/config.php';
	include 'lib/Database.php';
	
	$db = new Database();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>File Upload</title>
</head>
<body>
	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
    		$permited  = array('jpg', 'jpeg', 'png', 'gif');

      		$file_name = $_FILES["images"]["name"];
      		
      		$file_size = $_FILES["images"]["size"];
      		
      		$file_temp = $_FILES["images"]["tmp_name"];
      		
      		$folder ="uploads/";
      		move_uploaded_file($file_temp, $folder.$file_name);
      		$query ="INSERT INTO tbl_img(images) VALUES($file_name)";
      		$inserted_row = $db->insert($query);
      		if ($inserted_row) {
      			echo "Image inserted Successfully";
      		}else{
      			echo "image not inserted";
      		}
		}
	?>

	<form action="" method="POST" enctype="multipart/form-data">
		<table>
			<tr>
				<td>Select Image</td>
				<td><input type="file" name="images"/></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="submit" value="upload" /></td>
			</tr>
		</table>
	</form>
</body>
</html>